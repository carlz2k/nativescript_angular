# NativeScript Proof of Concept

native script
1) es6 supported (via babel), but not enough tutorial/samples as ones in type script, requires some hacks because of decorators and types, but manageable
2) angular 2 integration supported (native-angular)
3) css / sass supported (limited)
4) full access to native api (as they have claimed)
5) free version has limited UI elements, need to pay for more advanced components (such as charts)
6) no web app integration yet (they claim that they are working on it)
7) can work with third party native libraries if need to

So going forward, upgrade to angular 2 needs to be done first, and need to decide between es6/7 vs typescript

This repo serves as the starting point for NativeScript’s [JavaScript Getting Started Guide](https://docs.nativescript.org/tutorial/chapter-0).

Please file any issues with this template on the [NativeScript/docs repository](https://github.com/nativescript/docs), which is where the tutorial content lives.