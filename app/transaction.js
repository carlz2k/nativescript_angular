import {Component} from "@angular/core";
import {Http, Headers} from '@angular/http'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';  // debug
import 'rxjs/add/operator/catch';
import appSettings from 'application-settings';

@Component({
    selector: "transactions",
    template: `
    <ActionBar title="Transactions"></ActionBar>
    <StackLayout orientation="vertical">
    <Button text="Back" class="button button-accent" [nsRouterLink]="['/']"></Button>

    <ListView [items]="model.transactions">
    <template let-item="item" let-i="index" let-odd="odd" let-even="even">
        <StackLayout>
            <Label [text]='item.id +"$" + item.amount'></Label>
        </StackLayout>
    </template>
</ListView>
</StackLayout>
  `
})

export class TransactionComponent {
    static get parameters() {
        return [[Http]];
    }

    constructor(http) {
        this.http = http;
        this.model = {};
    }

    ngOnInit() {
        let headers = new Headers();

        let token = appSettings.getString("token", "");

        headers.append('Referer', 'https://dev-test.payfirma.com/');
        headers.append('Accept', 'application/json, text/plain, */*');
        headers.append('Authorization', 'Bearer ' + token);

        let _url = 'https://dev-test-apigateway.payfirma.com/transaction-service-vt/transaction';

        console.log(this.http.post);
        return this.http.get(_url, {headers: headers})
            .map((response) => {
                return response.json();
            })
            .subscribe((response) => {
                //console.log("resp2 = " + response.transactions);
                this.model.transactions = response.transactions;
            }, (error) => {
                console.log("error = " + error);
            }, () => {
                console.log("completed");
            });


    }
}