import {Component} from '@angular/core';

@Component({
    selector: "my-app",
    template: `
    <StackLayout>
        <router-outlet></router-outlet>
    </StackLayout>
  `
})
export class AppComponent {
}
