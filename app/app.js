import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {platformNativeScriptDynamic} from "nativescript-angular/platform";
import {NativeScriptModule} from "nativescript-angular/nativescript.module";
import {NativeScriptRouterModule} from "nativescript-angular/router";
import {NativeScriptFormsModule} from "nativescript-angular/forms"
import {NativeScriptHttpModule} from "nativescript-angular/http"
import {AppComponent} from "./app.component";
import {LoginComponent} from "./login";
import {TransactionComponent} from "./transaction";


export const appRoutes = [
    {path: "", component: LoginComponent},
    {path: "transactions", component: TransactionComponent}
];

export const appComponents = [
    LoginComponent,
    TransactionComponent
];

@NgModule({
    declarations: [AppComponent, ...appComponents],
    schemas: [NO_ERRORS_SCHEMA],
    bootstrap: [AppComponent],
    imports: [
        NativeScriptModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forRoot(appRoutes)
    ],
})
export class AppModule {
}

platformNativeScriptDynamic().bootstrapModule(AppModule);