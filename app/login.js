import {Component} from "@angular/core";
import {Router} from '@angular/router';
import {Http, Headers, URLSearchParams} from '@angular/http'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';  // debug
import 'rxjs/add/operator/catch';
import appSettings from 'application-settings';

@Component({
    selector: "login",
    template: `
    <ActionBar title="Login"></ActionBar>
    <StackLayout>
    <StackLayout orientation="vertical">
        <Label text='Username'></Label>
        <TextField [(ngModel)]='model.username'></TextField>
        <Label text="Password"></Label>
        <TextField [(ngModel)]='model.password' secure='true'></TextField>
        <Button text='Login' (tap)='login()'></Button>
    </StackLayout>
    <router-outlet></router-outlet>
     </StackLayout>
  `
})

export class LoginComponent {
    static get parameters() {
        return [[Http], [Router]];
    }

    constructor(http, router) {
        this.router = router;
        this.http = http;
        this.model = {};
    }

    login() {
        let headers = new Headers();

        headers.append('Referer', 'https://dev-test.payfirma.com/');
        headers.append('Accept', 'application/json, text/plain, */*');
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        let _urlParams = new URLSearchParams();
        _urlParams.set('username', encodeURIComponent(this.model.username));
        _urlParams.set('grant_type', 'password');
        _urlParams.set('password', encodeURIComponent(this.model.password));
        _urlParams.set('client_id', 'fc47a4dc1ec381c321f98312fe0444c0');

        let _url = 'https://dev-test-auth.payfirma.com/payhq/token';

        //console.log(this.http.post);
        return this.http.post(_url, _urlParams, {headers: headers})
            .map(function (response) {
                return response.json();
            })
            .subscribe((response) => {
                //console.log("resp2 = " + response.access_token);
                appSettings.setString('token', response.access_token);
                this.router.navigate(['/transactions']);
            }, function (error) {
                console.log("error = " + error);
            }, function () {
                console.log("completed");
            });


    }
}
